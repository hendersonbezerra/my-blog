# Modificar o comportamento padrão para evitar a criação de divs extras
ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  html_tag.html_safe
end