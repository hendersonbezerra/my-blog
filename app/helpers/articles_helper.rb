module ArticlesHelper
  def truncate_text(text)
    truncate(text, length: 250, separator: "...")
  end

  def publication_date(e)
    e.strftime("%B %e,%Y")
  end
  
end
